def FAILED_STAGE
def BUILD_NUMBER

pipeline 
 {
    agent any
    
	tools
	{
		jdk 'JAVA_HOME'
	} 
	options
	{
		buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '10'))
		
	}
	
	stages
	{
	    stage('code checkout')
		{
		 steps
			{
			 script{
			     
			git([url: 'https://u547775@bitbucket.org/u547775/pingfed.git', branch: 'master', credentialsId: 'eaecdcfe-27fc-41fa-8c1f-e90b64e820ef'])
			  }
             }			  
		}
	         
	stage('Install PingFed Admin')
		{
		  agent {label 'PF_ST' }
	       steps
		   {
		       sh "java -version"
		       sh "whoami"
		       sh "export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk"
			   sh "echo taking backup of previous file"
			   sh "sudo rm -rfv /opt/Ping/PFadmin"
			   sh "echo Creating Admin dir"
		       sh "sudo mkdir /opt/Ping/PFadmin"
			   sh "echo Copying zip file"
		       sh "sudo cp -R /var/lib/Jenkins/workspace/Installation_PingFed/pingfederate-9.3.3.zip /opt/Ping/PFadmin/"
			   sh "echo Unzip Pingfed zip file"
			   sh "sudo unzip /opt/Ping/PFadmin/pingfederate-9.3.3.zip -d  /opt/Ping/PFadmin"
			   sh "cd /opt/Ping/PFadmin/pingfederate-9.3.3/pingfederate/sbin"
			   sh "echo Running PingFed admin services"
			   sh "sudo /opt/Ping/PFadmin/pingfederate-9.3.3/pingfederate/sbin/pingfederate-run.sh"
			   sh "echo Service check"
			   sh "ps -ef |grep ping"
	       }
         } 
		 
		 stage('Install PingFed Engine')
		{
		  agent {label 'PF_ST' }
	       steps
		   {
		       sh "java -version"
		       sh "whoami"
		       sh "export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk"
			   sh "echo taking backup of previous file"
			   sh "echo Deleting previous version"
		       sh "echo Creating Engine dir"
			   sh "sudo mkdir /opt/Ping/PFengine"
		       sh "echo Copying zip file"
			   sh "sudo cp -R /var/lib/Jenkins/workspace/Installation_PingFed/pingfederate-9.3.3.zip /opt/Ping/PFengine/"
			   sh "echo Unzip Pingfed zip file"
			   sh "sudo unzip /opt/Ping/PFengine/pingfederate-9.3.3.zip -d /opt/Ping/PFengine"
			   sh "ifconfig"
			   sh "cd /opt/Ping/PFengine/pingfederate-9.3.3/pingfederate/sbin"
			   sh "echo Running PingFed Engine services "
			   sh "sudo /opt/Ping/PFengine/pingfederate-9.3.3/pingfederate/sbin/pingfederate-run.sh"
			   sh "echo Service check"
			   sh "ps -ef |grep ping"
	       }
        }
		stage('Install PingFed Engine 2')
		{  agent {label 'PF_Admin' }
	       steps
		   {
		    sh "sudo scp -r /opt/Ping/PFengine/ ec2-user@10.80.135.10:/tmp"
            sh "sudo ssh -t ec2-user@10.80.135.10  ls /tmp"
            sh "sudo ssh -t ec2-user@10.80.135.10  sudo cp -R /tmp/PFengine /opt/Ping/"
			sh "sudo ssh -t ec2-user@10.80.135.10  sudo cp -R /opt/Ping/PFengine/pingfederate-9.3.3/ /opt/Pinfed_engine_bkp/"
            sh "sudo ssh ec2-user@10.80.135.10  sudo /opt/Ping/PFengine/pingfederate-9.3.3/pingfederate/sbin/pingfederate-run.sh"
            sh "sudo ssh ec2-user@10.80.135.10  sudo ps -ef | grep ping"
		   }
         }
	}		
  }  